const { insert, getAll } = require("./questions.service");

async function insertQuestion(req, res) {
  const data = await insert(req.body);
  res.json({
    status: 200,
    message: data,
  });
}

async function getAllQuestion(req, res) {
  const response = await getAll();
  res.json({
    status: 200,
    message: response,
  });
}

module.exports = {
  insertQuestion,
  getAllQuestion,
};

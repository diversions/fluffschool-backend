const router = require("express").Router();
const asyncMiddleware = require("../middlewares/asyncMiddleware");
const controller = require("./questions.controller");
router.route("/insert").post(asyncMiddleware(controller.insertQuestion));
router.route("/insertAll").get(asyncMiddleware(controller.getAllQuestion));
module.exports = router;

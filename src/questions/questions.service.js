const { Question } = require("../db/models/Question");
function insert(data) {
  return Question.create(data);
}

function getAll() {
  return Question.find();
}

module.exports = {
  insert,
  getAll,
};

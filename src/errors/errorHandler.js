function errorHandler(error, request, response, next) {
  const { status = 500, message = "something went wrong!" } = error;
  response.status(status).json({ error: message });
}

module.exports = errorHandler;

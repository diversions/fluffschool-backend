const express = require("express");
const morgan = require("morgan");
const errorHandler = require("./errors/errorHandler");
const notFound = require("./errors/notFound");
const app = express();
const fluffRouter = require("./fluff/fluff.router");
const userRouter = require("./users/users.router");
const questionRouter = require("./questions/questions.router");
const optionRouter = require("./options/options.router");
const answerRouter = require("./answers/answers.router");
// app.use(express.urlencoded());
app.use(express.json());
app.use("/fluff", fluffRouter);
app.use("/users", userRouter);
app.use("/questions", questionRouter);
app.use("/options", optionRouter);
app.use("/answers", answerRouter);
app.use(morgan("dev"));
app.use(notFound);
app.use(errorHandler);

module.exports = app;

const router = require("express").Router();
const controller = require("./fluff.controller");
router.route("/dark").get(controller.getFluff);
module.exports = router;

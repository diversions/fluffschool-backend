const router = require("express").Router();
const controller = require("./users.controller");
router.route("/add/:id").post(controller.adduser);

module.exports = router;

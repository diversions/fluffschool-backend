const { insertUser } = require("./users.service");

async function adduser(req, res) {
  try {
    const data = await insertUser(req.params);
    console.log(req.body);
    res.json({
      status: 200,
      message: data,
    });
  } catch (error) {
      next({
          status: 500,
          message: error
      })
  }
}

module.exports = {
  adduser,
};

const {Answer} = require('../db/models/Answer');
function insert(data) {
    return Answer.create(data)
}

function insertMany(data) {
    return Answer.insertMany(data)
}

module.exports = {
    insert, 
    insertMany
}
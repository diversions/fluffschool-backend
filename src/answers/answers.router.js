
const router = require('express').Router();
const asyncMiddleware = require('../middlewares/asyncMiddleware');
const controller = require('./answers.controller')
router.route('/insert').post(asyncMiddleware(controller.insertAnswer))
router.route('/insertAll').get(asyncMiddleware(controller.insertAllAnswer))
module.exports = router;
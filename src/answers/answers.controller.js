const { insert, insertMany } = require("./answers.service")

async function insertAnswer(req, res) {
    const data = await insert(req.body);
    res.json({
        status: 200,
        message: data
    })
}

async function insertAllAnswer(req, res) {
    const response = await insertMany(req.body.answers);
    res.json({
        status: 200,
        message: response
    })
}

module.exports = {
    insertAnswer,
    insertAllAnswer
}
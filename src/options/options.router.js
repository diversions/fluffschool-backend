const router = require("express").Router();
const asyncMiddleware = require("../middlewares/asyncMiddleware");
const controller = require("./options.controller");
router.route("/insert").get(asyncMiddleware(controller.insertOne));
router.route("./insertMany").get(asyncMiddleware(controller.insertAll));
module.exports = router;

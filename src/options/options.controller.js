const { insert, insertMany } = require("./options.service");

async function insertOne(req, res) {
    const data = await insert(req.body);
    res.json({
        status: 200,
        message: data
    })
}

async function insertAll(req, res) {
    const data = await insertMany(data);
    res.json({
        status: 200,
        message: response
    })
}

module.exports = {
    insertOne,
    insertAll
}
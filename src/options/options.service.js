const {Option} = require('../db/models/Option');
function insert(data) {
    return Option.insert(data)
}
function insertMany(data) {
    return Option.insertMany(data)
}

module.exports = {
    insert,
    insertMany
}
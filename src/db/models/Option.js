const mongoose = require("mongoose");
const OptionSchema = mongoose.Schema({
  questionID: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  option: {
    type: String,
    required: true,
  },
  created_date: {
    type: Date,
    default: Date.now,
  },
});
const Option = mongoose.model("Option", OptionSchema)
module.exports = {
    Option
}
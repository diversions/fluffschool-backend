const mongoose = require("mongoose");
const QuestionSchema = mongoose.Schema({
  question: {
    type: String,
    required: true,
  },
  created_date: {
    type: Date,
    default: Date.now,
  },
});
const Question = mongoose.model("Question", QuestionSchema)
module.exports = {
    Question
}
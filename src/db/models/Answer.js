const mongoose = require("mongoose");
const AnswerSchema = mongoose.Schema({
  userID: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  questionID: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  answer: {
    type: String,
    required: true,
  },
  created_date: {
    type: Date,
    default: Date.now,
  },
});
const Answer = mongoose.model('Answer', AnswerSchema)
module.exports = {
  Answer
}
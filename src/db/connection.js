const mongoose = require("mongoose");
mongoose.connect(
  "mongodb+srv://fluffschool:schoolfluff@cluster0.8zzgh.mongodb.net/fluffschool?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
  }
);
const mongo = mongoose.connection;
if (!mongo) {
  console.log("Error connecting db");
} else {
  console.log("Db Connected successfully");
}
module.exports = mongo;